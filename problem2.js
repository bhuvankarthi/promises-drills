const fs = require("fs" ) ;


function problem2(){
  const answersPath ="./answers"
fs.promises.mkdir(answersPath)
.then(()=>{
  fs.promises.readFile("./lipsum.txt","utf-8")
  .then((lipsumData)=>{
    const upper = "upper.txt"
    const upperCase = lipsumData.toUpperCase()
    fs.promises.appendFile(`${answersPath}/${upper}`,upperCase)
    .then(()=>{
      const lower = "lower.txt"
      const lowerCase = lipsumData.toLowerCase().slice(0,lipsumData.length-1).split(".").join("*")
      fs.promises.appendFile(`${answersPath}/${lower}`,lowerCase)
      .then(()=>{
        const sortedData= (upperCase + lowerCase.split("*").join("")).split("").sort();
        const sortedPath =  "sorted.txt"
        fs.promises.appendFile(`${answersPath}/${sortedPath}` ,sortedData )
        .then(()=>{
          console.log("hiiii");
          fs.promises.appendFile("./filesname.txt" , upper+"*"+lower+"*"+sortedPath )
          .then(()=>{
            fs.promises.readFile("./filesname.txt","utf-8")
            .then((filenamesData)=>{
              const array = filenamesData.split("*").forEach((file)=>{
                fs.unlink(`${answersPath}/${file}`,(err)=>{
                  if(err){
                    console.log(err);
                  }
                })
              })
            }).catch((err)=>{
              console.log(err);
            })
          
          }).catch((err)=>{
            console.log(err);
          })

        }).catch((err)=>{
          console.log(err);
        })

      }).catch((err)=>{
        console.log(err);
      })
        
          
    }).catch((err)=>{
      console.log(err);
    })

  }).catch((err)=>{
    console.log(err);
  })

}).catch((err)=>{
  console.log(err);
})

}

  
  
module.exports = problem2

